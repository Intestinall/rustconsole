#[macro_use]
extern crate lazy_static;

mod art;
pub mod child_with_args;
mod command;
pub mod errors;
pub mod jobs;
mod run;
pub mod sub_command;
pub mod token;
pub mod utils;
pub mod working_dir;

use crate::jobs::Jobs;
use crate::run::run;
use crate::working_dir::WorkingDir;
use std::process::exit;
use std::time::Instant;

fn main() {
    art::print_welcome();
    let mut user_input: String = String::with_capacity(256);
    let mut start_time = Instant::now();
    let mut no_command = true;

    let working_dir = match WorkingDir::new() {
        Ok(v) => v,
        Err(_) => {
            println!("rcl: FATAL ERROR: cannot find current working directory");
            exit(1)
        }
    };
    let mut jobs = Jobs::new();

    loop {
        utils::read_line(&mut user_input, start_time, no_command, &working_dir);
        let trimmed_input = user_input.trim();

        start_time = Instant::now();
        no_command = trimmed_input.is_empty();

        if !trimmed_input.is_empty() {
            if let Err(e) = run(trimmed_input, &working_dir, &mut jobs) {
                println!("{}", e);
            }
        }
        user_input.clear();
    }
}
