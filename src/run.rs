use crate::command::ShellCommand;
use crate::jobs::Jobs;
use crate::token::Token;
use crate::working_dir::WorkingDir;
use std::error::Error;
use std::process::exit;

pub fn run(
    trimmed_input: &str,
    working_dir: &WorkingDir,
    mut jobs: &mut Jobs,
) -> Result<(), Box<dyn Error>> {
    let mut shell_command = ShellCommand::new(trimmed_input)?;

    while !shell_command.sub_commands.is_empty() {
        let mut sub_command = shell_command.pop_back()?;
        if &sub_command.command_name == "exit" {
            exit(0)
        };

        match &sub_command.token {
            Token::Background => {
                if let Some(child) = sub_command.background_run(working_dir, &mut jobs)? {
                    jobs.push(child, &sub_command)?;
                }
                break;
            }
            Token::Pipe => shell_command.pipe_unless(sub_command, working_dir, jobs)?,
            Token::And | Token::Or | Token::None | Token::AndDo => {
                sub_command.foreground_run(working_dir, &mut jobs)?;
                if sub_command.is_termination() {
                    return Ok(());
                }
            }
            _ => unreachable!("Should have been checked at Token creation"),
        }
    }
    Ok(())
}
