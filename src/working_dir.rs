use std::collections::VecDeque;
use std::env::current_dir;
use std::error::Error;
use std::path::PathBuf;
use std::sync::{RwLock, RwLockWriteGuard};

use dirs::home_dir;

use crate::errors::RustConsoleError;
use crate::sub_command::SubCommand;

pub struct WorkingDir {
    inner: RwLock<PathBuf>,
    // TODO : Recent paths
}

impl WorkingDir {
    pub fn new() -> Result<WorkingDir, Box<dyn Error>> {
        match current_dir() {
            Ok(v) => Ok(WorkingDir {
                inner: RwLock::new(v),
            }),
            Err(_) => Err(RustConsoleError::WorkingDirectoryError.into()),
        }
    }

    pub fn is_cd(sub_command: &SubCommand) -> bool {
        &sub_command.command_name == "cd"
    }

    pub fn get_path(&self) -> Result<PathBuf, Box<dyn Error>> {
        match self.inner.read() {
            Ok(v) => Ok((*v.clone()).to_path_buf()),
            Err(_) => Err(RustConsoleError::WorkingDirectoryError.into()),
        }
    }

    pub fn get_path_no_error(&self) -> String {
        match self.get_path() {
            Ok(v) => format!("{}", v.display()),
            Err(_) => "<unknown>".to_string(),
        }
    }

    fn handle_one_argument(
        &self,
        path: &str,
        w: &mut RwLockWriteGuard<PathBuf>,
    ) -> Result<bool, Box<dyn Error>> {
        match (**w).join(path).to_path_buf().canonicalize() {
            Ok(path) => match path.is_dir() {
                true => {
                    **w = path;
                    Ok(true)
                }
                false => Err(RustConsoleError::CommandRunError(
                    "cd".to_string(),
                    format!("not a directory: {}", path.display()),
                )
                .into()),
            },
            Err(_) => Err(RustConsoleError::CommandRunError(
                "cd".to_string(),
                format!("no such file or directory: {}", path),
            )
            .into()),
        }
    }

    pub fn update_path(&self, args: &VecDeque<String>) -> Result<bool, Box<dyn Error>> {
        let mut w = match self.inner.write() {
            Ok(v) => v,
            Err(_) => return Err(RustConsoleError::WorkingDirectoryError.into()),
        };
        match args.len() {
            0 => {
                let home = self.get_home()?;
                *w = home;
                Ok(true)
            }
            1 => Ok(self.handle_one_argument(&args[0], &mut w)?),
            _ => {
                return Err(RustConsoleError::CommandRunError(
                    "cd".to_string(),
                    "too many arguments".to_string(),
                )
                .into())
            }
        }
    }

    fn get_home(&self) -> Result<PathBuf, Box<dyn Error>> {
        match home_dir() {
            Some(path) => Ok(path),
            None => Err(RustConsoleError::CommandRunError(
                "cd".to_string(),
                "cannot find home directory".to_string(),
            )
            .into()),
        }
    }
}
