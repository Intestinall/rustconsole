use crate::working_dir::WorkingDir;
use chrono::Local;
use colored::*;
use std::env;
use std::io::{self, Write};
use std::process::Command;
use std::time::Instant;

/// Wrapper for io::stdin::read_line method with
/// error management and pretty shell printing
pub fn read_line(s: &mut String, start_time: Instant, no_command: bool, working_dir: &WorkingDir) {
    print!("{}", console_symbol(start_time, no_command, working_dir));
    if let Err(e) = io::stdout().flush() {
        println!("rcl unrecoverable error: {}", e)
    }

    let stdin = io::stdin();
    if let Err(e) = stdin.read_line(s) {
        println!("rcl unrecoverable error: {}", e)
    }
}

/// Display a pretty shell with the following informations :
///     - username
///     - hostname
///     - current working directory
///     - current datetime
///     - last command execution time
fn console_symbol(start_time: Instant, no_command: bool, working_dir: &WorkingDir) -> String {
    let user = match env::var("USER") {
        Ok(v) => v,
        Err(_) => "<no_username_found>".to_string(),
    };
    let hostname = match Command::new("hostname").output() {
        Ok(v) => String::from_utf8_lossy(&v.stdout).into_owned(),
        Err(_) => "<no_hostname_found>".to_string(),
    };
    let datetime: &str = &Local::now().format("%a %b%e, %T").to_string();

    let last_command_duration = if no_command {
        "".to_string()
    } else {
        match command_duration(start_time).as_str() {
            "" => "".to_string(),
            t => format!(
                "{hyphen} {opening_bracket}{last_command_duration}{closing_bracket}",
                hyphen = "-".white().bold(),
                opening_bracket = " [".bright_blue().bold(),
                last_command_duration = t.yellow().bold(),
                closing_bracket = "] ".bright_blue().bold(),
            ),
        }
    };

    format!(
        "{}{opening_bracket}{user}{}{hostname}{closing_bracket} {hyphen} {opening_bracket}{}{closing_bracket} {hyphen} {opening_bracket}{datetime}{closing_bracket}{last_command_duration}{}",
        "┌─".bright_blue().bold(),
        "@".black().bold(),
        working_dir.get_path_no_error(),
        "\n└─<> ".bright_blue().bold(),
        opening_bracket = "[".bright_blue().bold(),
        closing_bracket = "]".bright_blue().bold(),
        hyphen = "-".white().bold(),
        user = user.green().bold(),
        hostname = hostname.trim().cyan().bold(),
        datetime = datetime.black().bold(),
        last_command_duration = if no_command { "".to_string() } else { last_command_duration },
    )
}

/// Compute last command execution time
fn command_duration(start_time: Instant) -> String {
    let mut milliseconds = Instant::now().duration_since(start_time).as_millis() as f64;
    let mut seconds = milliseconds / 1000.0;
    milliseconds %= 1000.0;
    let mut minutes = seconds / 60.0;
    seconds %= 60.0;
    let hours = minutes / 60.0;
    minutes %= 60.0;

    let values = [hours, minutes, seconds, milliseconds];
    let value_letters = ["h", "m", "s", "ms"];
    let mut finish_him = false;
    let mut v: Vec<String> = Vec::with_capacity(4);

    for (value, letter) in values.iter().zip(value_letters.iter()) {
        let value = *value as usize;
        match finish_him {
            true => v.push(format!("{}{}", value, letter)),
            false => match value {
                0 => continue,
                _ => {
                    finish_him = true;
                    v.push(format!("{}{}", value, letter))
                }
            },
        }
    }
    v.join(" ")
}
