use std::collections::VecDeque;

use regex::Regex;

use crate::errors::RustConsoleError;
use crate::jobs::Jobs;
use crate::sub_command::SubCommand;
use crate::token::Token;
use crate::working_dir::WorkingDir;
use std::error::Error;
use std::str::FromStr;

/// Represent the full user input consisting
/// in a mix of command, args and tokens.
pub struct ShellCommand {
    pub sub_commands: VecDeque<SubCommand>,
}

lazy_static! {
    /// User input parsing regex, it handles :
    ///    - commands and args
    ///    - spaces between double quotes
    ///    - tokens
    static ref SHELL_REGEX: Regex =
        Regex::new(r#"[/.\w-]+|".+"+|[>&|<;]{1,2}"#).expect("The regex crashed?!");
}

impl ShellCommand {
    const SHELL_COMMANDS_SUB_COMMAND_SIZE: usize = 16;
    const SUB_COMMANDS_ARGS_SIZE: usize = 16;

    pub fn new(input: &str) -> Result<ShellCommand, Box<dyn Error>> {
        let mut self_ = ShellCommand::_new()?;
        let args = ShellCommand::parse_input(input);
        let mut current_args: VecDeque<String> =
            VecDeque::with_capacity(ShellCommand::SUB_COMMANDS_ARGS_SIZE);

        for arg in args {
            if Token::is_token(&arg) {
                let sub_command = SubCommand::new(current_args, Token::from_str(&arg)?)?;
                current_args = VecDeque::with_capacity(ShellCommand::SUB_COMMANDS_ARGS_SIZE);
                self_.sub_commands.push_front(sub_command);
            } else {
                current_args.push_front(arg);
            }
        }
        if !current_args.is_empty() {
            let sub_command = SubCommand::new(current_args, Token::None)?;
            self_.sub_commands.push_front(sub_command);
        }
        Ok(self_)
    }

    /// Hidden new to reduce boilerplate of the main one
    fn _new() -> Result<ShellCommand, Box<dyn Error>> {
        Ok(ShellCommand {
            sub_commands: VecDeque::with_capacity(ShellCommand::SHELL_COMMANDS_SUB_COMMAND_SIZE),
        })
    }

    /// Iterator containing the user input parsed with SHELL_REGEX
    fn parse_input(input: &str) -> impl Iterator<Item = String> + '_ {
        SHELL_REGEX
            .find_iter(input)
            .map(|x| x.as_str().trim_matches(|c| c == '"').to_string())
    }

    /// Properly pipe stdout and stdin of command unless
    /// it found a command with a non-pipe token
    pub fn pipe_unless(
        &mut self,
        mut sub_command: SubCommand,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<(), Box<dyn Error>> {
        while sub_command.token.is(Token::Pipe) && !self.sub_commands.is_empty() {
            sub_command.pipe_out();
            let mut next_sub_command = self.pop_back()?;
            next_sub_command.pipe_in(sub_command, working_dir, jobs)?;
            sub_command = next_sub_command;
        }
        self.pipe_end(sub_command, working_dir, jobs)?;
        Ok(())
    }

    /// Handle special case of last piped command
    /// and wait for its output before printing it
    fn pipe_end(
        &self,
        mut last_sub_command: SubCommand,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<(), Box<dyn Error>> {
        let child = match last_sub_command.background_run(working_dir, jobs)? {
            Some(v) => v,
            None => {
                return Err(RustConsoleError::CommandRunError(
                    "cd".to_string(),
                    "cannot be piped".to_string(),
                )
                .into())
            }
        };
        let output = child.wait_with_output()?;
        print!("{}", String::from_utf8_lossy(&output.stdout));
        Ok(())
    }

    /// Passthrough for std::collections::VecDeque::pop_back method
    pub fn pop_back(&mut self) -> Result<SubCommand, Box<dyn Error>> {
        match self.sub_commands.pop_back() {
            Some(v) => Ok(v),
            None => Err(RustConsoleError::RustConsoleInternalError(
                "Something wrong happened...".to_string(),
            )
            .into()),
        }
    }
}
