use crate::sub_command::SubCommand;
use std::process::Child;

pub struct ChildWithArgs {
    /// Wrapper around std::process::Child to facilitate
    /// access to its command_name and args for jobs and kill commands
    pub inner: Child,
    pub command_name: String,
    pub string_args: String,
}

impl ChildWithArgs {
    pub fn new(child: Child, sub_command: &SubCommand) -> ChildWithArgs {
        let owned_args: Vec<String> = sub_command.args.iter().map(|x| x.to_string()).collect();
        ChildWithArgs {
            inner: child,
            command_name: sub_command.command_name.to_owned(),
            string_args: owned_args.join(" "),
        }
    }

    /// Passthrough for std::process::Child::id method
    pub fn id(&self) -> u32 {
        self.inner.id()
    }

    /// Proper display for the original command that yield this child
    pub fn display_command(&self) -> String {
        format!("{} {}", self.command_name, self.string_args)
    }
}
