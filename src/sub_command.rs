use crate::errors::RustConsoleError;
use crate::jobs::Jobs;
use crate::token::Token;
use crate::working_dir::WorkingDir;
use std::collections::VecDeque;
use std::error::Error;
use std::process::{Child, Command, Stdio};

pub struct SubCommand {
    pub command: Command,
    pub command_name: String,
    pub args: VecDeque<String>,
    pub token: Token,
    pub success: bool,
}

impl SubCommand {
    pub fn new(mut args: VecDeque<String>, token: Token) -> Result<SubCommand, Box<dyn Error>> {
        let command_name = match args.pop_back() {
            Some(v) => v,
            None => {
                return Err(Box::new(RustConsoleError::SyntaxError(
                    token.as_str().to_string(),
                )))
            }
        };
        let mut command = Command::new(&command_name);
        command.args(&args);
        Ok(SubCommand {
            command,
            command_name,
            args,
            token,
            success: false,
        })
    }

    fn handle_specials(
        &mut self,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<Option<bool>, Box<dyn Error>> {
        if WorkingDir::is_cd(&self) {
            return Ok(Some(working_dir.update_path(&self.args)?));
        } else if Jobs::is_jobs(&self) {
            return Ok(Some(jobs.display()?));
        } else if Jobs::is_kill(&self) {
            return Ok(Some(jobs.kill(&self.args)?));
        }
        Ok(None)
    }

    pub fn foreground_run(
        &mut self,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<(), Box<dyn Error>> {
        if let Some(success) = self.handle_specials(working_dir, jobs)? {
            self.success = success;
            return Ok(());
        }

        self.command.current_dir(&working_dir.get_path()?);
        match self.command.status() {
            Ok(v) => {
                self.success = v.success();
                Ok(())
            }
            Err(e) => {
                self.success = false;
                Err(Box::new(RustConsoleError::CommandRunError(
                    self.command_name.to_string(),
                    e.to_string(),
                )))
            }
        }
    }

    pub fn background_run(
        &mut self,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<Option<Child>, Box<dyn Error>> {
        if let Some(success) = self.handle_specials(working_dir, jobs)? {
            self.success = success;
            return Ok(None);
        }

        self.command.current_dir(&working_dir.get_path()?);
        match self.command.spawn() {
            Ok(v) => Ok(Some(v)),
            Err(e) => {
                self.success = false;
                Err(Box::new(RustConsoleError::CommandRunError(
                    self.command_name.to_string(),
                    e.to_string(),
                )))
            }
        }
    }

    pub fn pipe_in(
        &mut self,
        mut parent: SubCommand,
        working_dir: &WorkingDir,
        jobs: &mut Jobs,
    ) -> Result<(), Box<dyn Error>> {
        let parent_spawn = match parent.background_run(working_dir, jobs)? {
            Some(v) => v,
            None => {
                return Err(RustConsoleError::CommandRunError(
                    "cd".to_string(),
                    "cannot be piped".to_string(),
                )
                .into())
            }
        };
        let parent_stdout = match parent_spawn.stdout {
            Some(v) => v,
            None => {
                return Err(Box::new(RustConsoleError::PipeError(
                    self.command_name.to_string(),
                )))
            }
        };
        self.command.stdin(parent_stdout);
        Ok(())
    }

    pub fn pipe_out(&mut self) {
        self.command.stdout(Stdio::piped());
    }

    pub fn is_termination(&self) -> bool {
        match &self.token {
            Token::AndDo => false,
            Token::None => true,
            Token::And => !self.success,
            Token::Or => self.success,
            _ => true,
        }
    }
}
