use crate::child_with_args::ChildWithArgs;
use crate::errors::RustConsoleError;
use crate::sub_command::SubCommand;
use std::collections::{HashMap, VecDeque};
use std::error::Error;
use std::process::Child;
use std::sync::{RwLock, RwLockWriteGuard};

pub struct Jobs {
    inner: RwLock<HashMap<u32, ChildWithArgs>>,
}

impl Jobs {
    const JOBS_SIZE: usize = 16;

    pub fn new() -> Jobs {
        Jobs {
            inner: RwLock::new(HashMap::with_capacity(Jobs::JOBS_SIZE)),
        }
    }

    pub fn is_jobs(sub_command: &SubCommand) -> bool {
        &sub_command.command_name == "jobs"
    }

    pub fn is_kill(sub_command: &SubCommand) -> bool {
        &sub_command.command_name == "kill"
    }

    fn get_finished_childs(
        &self,
        w: &mut RwLockWriteGuard<HashMap<u32, ChildWithArgs>>,
    ) -> Vec<u32> {
        let mut ids_to_delete: Vec<u32> = Vec::with_capacity(Jobs::JOBS_SIZE);
        for child in (*w).values_mut() {
            let wait = child.inner.try_wait();
            if wait.is_err() || wait.unwrap().is_some() {
                ids_to_delete.push(child.id());
            }
        }
        ids_to_delete
    }

    pub fn refresh(&self) -> Result<(), Box<dyn Error>> {
        match self.inner.write() {
            Ok(mut w) => {
                for id in self.get_finished_childs(&mut w) {
                    (*w).remove(&id);
                }
            }
            Err(_) => return Err(RustConsoleError::JobsError.into()),
        }
        Ok(())
    }

    pub fn push(&self, child: Child, sub_command: &SubCommand) -> Result<(), Box<dyn Error>> {
        match self.inner.write() {
            Ok(mut w) => {
                (*w).insert(child.id(), ChildWithArgs::new(child, sub_command));
                Ok(())
            }
            Err(_) => Err(RustConsoleError::JobsError.into()),
        }
    }

    pub fn display(&self) -> Result<bool, Box<dyn Error>> {
        match self.inner.read() {
            Ok(w) => {
                println!(
                    "{}",
                    w.values()
                        .enumerate()
                        .map(|(i, c)| self.format_jobs_line(i, c))
                        .collect::<Vec<String>>()
                        .join("\n")
                );
                Ok(true)
            }
            Err(_) => Err(RustConsoleError::JobsError.into()),
        }
    }

    pub fn kill(&mut self, args: &VecDeque<String>) -> Result<bool, Box<dyn Error>> {
        let id = match args.len() {
            0 => {
                return Err(RustConsoleError::CommandRunError(
                    "kill".to_string(),
                    "not enough arguments".to_string(),
                )
                .into())
            }
            1 => match args[0].parse::<u32>() {
                Ok(v) => v,
                Err(_) => {
                    return Err(RustConsoleError::CommandRunError(
                        "kill".to_string(),
                        format!("\"{}\" is not a valid number", &args[0]),
                    )
                    .into())
                }
            },
            _ => {
                return Err(RustConsoleError::CommandRunError(
                    "kill".to_string(),
                    "too many arguments (multiple kill will be implemented soon)".to_string(),
                )
                .into())
            }
        };

        match self.inner.write() {
            Ok(mut w) => {
                match (*w).remove(&id) {
                    Some(child) => {
                        println!(
                            "Successfully terminated {} : \"{}\"",
                            id,
                            child.display_command()
                        );
                        return Ok(true);
                    }
                    None => {
                        return Err(RustConsoleError::CommandRunError(
                            "kill".to_string(),
                            format!("kill {} failed: no such process", id),
                        )
                        .into())
                    }
                };
            }
            Err(_) => Err(RustConsoleError::JobsError.into()),
        }
    }

    fn format_jobs_line(&self, i: usize, child: &ChildWithArgs) -> String {
        format!(
            "[{index}]  ({child_id})    running    {command}",
            index = i,
            child_id = child.id(),
            command = child.display_command()
        )
    }
}
