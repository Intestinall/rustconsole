use std::error::Error;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
/// Rcl error enum to facilitate error management
pub enum RustConsoleError {
    TokenNotImplementedError(String),
    SyntaxError(String),
    CommandRunError(String, String),
    RustConsoleInternalError(String),
    PipeError(String),
    WorkingDirectoryError,
    JobsError,
}

impl Display for RustConsoleError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let message = match self {
            RustConsoleError::TokenNotImplementedError(e) => {
                format!("rcl: \"{}\" is not implemented yet !", e)
            }
            RustConsoleError::SyntaxError(e) => format!("rcl: syntax error near `{}'", e),
            RustConsoleError::CommandRunError(command, e) => format!("{}: {}", command, e),
            RustConsoleError::RustConsoleInternalError(s) => format!("rcl internal error: {}", s),
            RustConsoleError::PipeError(s) => format!("rcl: can't process \"{}\" stdout", s),
            RustConsoleError::WorkingDirectoryError => {
                format!("rcl internal error: unable to access current working directory")
            }
            RustConsoleError::JobsError => format!("rcl internal error: unable to access to jobs"),
        };
        write!(f, "{}", message)
    }
}

impl Error for RustConsoleError {}
