use crate::errors::RustConsoleError;
use std::fmt;
use std::str::FromStr;

#[derive(PartialEq)]
/// Human readable tokens for improved readability
pub enum Token {
    Pipe,
    Background,
    And,
    Or,
    AndDo,
    RightWriteRedirection,
    RightAppendRedirection,
    LeftWriteRedirection,
    LeftAppendRedirection,
    None,
}

impl Token {
    /// List of current regex recognized tokens
    const STR_TOKENS: [&'static str; 9] = ["|", "&", "&&", "||", ";", ">", ">>", "<", "<<"];

    /// Tell it the string is handled by the parse regex
    pub fn is_token(s: &str) -> bool {
        Token::STR_TOKENS.contains(&s)
    }

    /// Better token comparison
    pub fn is(&self, token: Token) -> bool {
        *self == token
    }

    pub fn as_str(&self) -> &str {
        match &self {
            Token::Pipe => "|",
            Token::Background => "&",
            Token::And => "&&",
            Token::Or => "||",
            Token::AndDo => ";",
            Token::RightWriteRedirection => ">",
            Token::RightAppendRedirection => ">>",
            Token::LeftWriteRedirection => "<",
            Token::LeftAppendRedirection => "<<",
            Token::None => "",
        }
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl FromStr for Token {
    type Err = RustConsoleError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "|" => Ok(Token::Pipe),
            "&" => Ok(Token::Background),
            "&&" => Ok(Token::And),
            "||" => Ok(Token::Or),
            ";" => Ok(Token::AndDo),
            c @ ">" | c @ ">>" | c @ "<" | c @ "<<" => {
                Err(RustConsoleError::TokenNotImplementedError(c.to_string()))
            }
            "" => Ok(Token::None),
            x => Err(RustConsoleError::SyntaxError(x.to_string())),
        }
    }
}
